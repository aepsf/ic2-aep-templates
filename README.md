# Salesforce DX Project: IC2-AEP-Templates

# Who may want to use this set of templates?

This set of templates were designed for those who have installed the following two libraries in there organizations.

https://github.com/apex-enterprise-patterns/fflib-apex-common

https://github.com/apex-enterprise-patterns/fflib-apex-mocks

# Lincense: All files in this repository are UNLICENSE.

# This project prepares a set of draft templates.

This set of draft templates may considered to be converted or transformed into some features of Illuminated Cloud 2 (IC2).

# How to use this before it has been converted into IC2 features.

## Currently this only supports Salesforce DX Source Format.

## Step 1: Install the above two libraries

### Option 1: Copy and paste the source files into the directory as below.

    force-app/main/default/classes/fflib

### Option 2: Install by using shane-sfdx-plugins

    echo y | sfdx plugins:install shane-sfdx-plugins
    
    sfdx force:org:create -f config/project-scratch-def.json --setdefaultusername -d 1
    
    sfdx shane:github:src:install -c -g apex-enterprise-patterns -r fflib-apex-mocks -p sfdx-source/apex-mocks
    
    sfdx shane:github:src:install -c -g apex-enterprise-patterns -r fflib-apex-mocks -p sfdx-source/apex-common
    
    sfdx force:source:push

## Step 2: Run Anonymous Apex to get a list of variables for the Python codes below

## Step 3: Transform and paste the list of variables for the Python codes below

## Step 4a: Run the Python codes to create a empty 3-Layer version (Opportunity Object) of Apex Classes and Trigger

## Step 4b: Run the Python codes to create a pre-filled 3-Layer version (CustomObject__c Object)  of Apex Classes and Trigger

## Step 4c: Run the Python codes to create a pre-filled Service Layer version (Case Object) of Apex Classes and Trigger

## Step 4d: Almost the same as step 4c, use a non-SObject name as the name of the Service Layer Class (e.g. Invoicing). Run the Python codes to create a pre-filled Service Layer version (non-SObject, e.g. Invoicing Service Class) of Apex Classes.

## Step 5: Use Illuminated Cloud 2 to deploy the created classes and Triggers together.
